#!/bin/bash

set -o errexit
set -o nounset

if [[ -z $(command -v docker) ]]; then
    echo 'Docksi requires docker to be installed. Please consult: https://docs.docker.com/install/'
    exit 1
fi

# Shortflags so the super modern OSX can use this...
echo "Creating ~/.local/docksi-bin"
mkdir -p ~/.local/docksi-bin

# Make sure we are using the latest version of docksi to install docksi
echo "Pulling registry.gitlab.com/kwilco/docksi docker image..."
docker pull registry.gitlab.com/kwilco/docksi >/dev/null

echo "Running docksi image to install docksi..."
docker run \
    --rm \
    --user "$(id -u):$(id -g)" \
    --volume ~/.local/docksi-bin:/.local/docksi-bin registry.gitlab.com/kwilco/docksi \
    install registry.gitlab.com/kwilco/docksi

echo 'Docksi has been installed at ~/.local/docksi-bin/docksi'
echo 'Useful Docksi commands:'
echo '  docksi install path/to/some-docker-image'
echo '  docksi list'
echo '  docksi uninstall docksi  (completely removes docksi or any other docksi-installed script)'

if [[ -z $(command -v docksi) ]]; then
    echo 'You will need to put ~/.local/docksi-bin on your shell $PATH'
    echo '  For bash, you can run: echo '\''PATH=$PATH:~/.local/docksi-bin'\''>>~/.bash_profile'
    echo '  For fish, you can run: set -U fish_user_paths ~/.local/docksi-bin $fish_user_paths'
    echo '  For other shells, you can probably figure it out!'
fi
