FROM python:alpine

WORKDIR /app

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY docksi.py /app

COPY docksi-run-args /

VOLUME ["/.local/docksi-bin"]

ENTRYPOINT ["/app/docksi.py"]
