# docksi

Docksi is a wrapper for docker-based CLI images. It installs a small wrapper
script for each image you install that auto-updates the image and provides a
hook to let that image specify extra `docker run` arguments, to make things
like mounting volumes and forwarding ports transparent.

Docksi stands for Docker Script Installer. The name and functionality are
heavily inspired by [pipsi](https://github.com/mitsuhiko/pipsi#pipsi).

# SECURITY WARNING

Running commands with the Docksi wrapper inspects an image for exposed volumes
and ports, and auto-mounts / auto-publishes them onto the host. This means
that a malicious image could craft a volume path that results in Docksi
mounting a sensitive path on the host. It is a **terrible idea to use Docksi
with any image you do not trust 100% to be friendly**. You have been warned.

See below about the `--docksi-dry-run` flag, which helps you see which volumes
and ports (if any) will be mounted or published for a particular image.

## Features

Why should you install Docksi?

* Easy installation of docker-based CLIs on your $PATH.
* Auto-updater for all installed images.
* Automatically mounts volumes and publishes ports from images. No need for a giant parameter salad every time you want to run something.

## Installation

Docksi requires `docker`. Please consult: https://docs.docker.com/install/

Docksi uses a small bash bootstrapper to install Docksi with Docksi. It does
not require root to run. I recommend that you download it, or clone the repo
and inspect it before running. It will install `docksi` in
`~/.local/docksi-bin`. This directory is where the scripts will be
installed, and will need to be added to your `$PATH`.

```bash
curl --silent https://gitlab.com/Kwilco/docksi/raw/master/get-docksi.sh | bash -
```

## Path

`~/.local/docksi-bin` must be on your shell's $PATH. Instructions are provided
upon running the installer, but they are repeated here:

* For bash, you can run: `echo "PATH=$PATH:~/.local/docksi-bin">>~/.bash_profile`
* For fish, you can run: `echo "set PATH $PATH ~/.local/docksi-bin">>~/.config/fish/config.fish`
* For other shells, you can probably figure it out!

## Usage

Run `docksi` to see the latest available commands. The important ones are documented here.

`docksi install`

This installs a docker image. The name of the command will be the name of the
docker image. Example:

```bash
$ docksi install example/docker-image
$ docker-image --help
```

Any image can be installed via Docksi, but it may or may not work depending on
how the image is made.

`docksi list`

Lists commands instaleld with Docksi.

`docksi uninstall`

Removes a command installed with docksi. Example:
```bash
$ docksi uninstall esv-gitlab-sync
```

## Docksi volume mounting and port publishing

If an installed images exposes volumes or ports, Docksi includes the
appropriate arguments in the `docker run` invocation to mount or publish
them.

The convention for volumes is: The volume path from the image is prefixed with
the user's home directory and mounted on the host. For instance, if the
exposed volume in an image is `/.vault-token`, then Docksi will produce the
following `docker run` flags:

```bash
--volume ~/.vault-token:/.vault-token
```

For ports, Docksi prefixes the port with "80" to publish it on the host. For
instance, "80/tcp" in the container image results in the following flags:

```bash
--publish 8080:80/tcp
```

To see what volume and port flags would be run for a particular image, you can
pass the `--docksi-dry-run` flag to any command that you have installed. That
does not actually execute the image, but instead displays what volume and port
flags would be passed to it during an actual run.


## Uninstallation

If you don't like `docksi`, you can remove it in one of the following ways:
* `docksi uninstall docksi`
* `~/.local/docksi-bin/docksi uninstall docksi`
* `rm -rf ~/.local/docksi-bin`
